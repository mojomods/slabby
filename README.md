# Slabby

Yet another vertical slabs mod, but give it a chance.

**Important: Fabric Only, Server and Client**

## Decoration Vanilla Slabs

Slabby creates slabs for common vanilla decoration blocks. These include: concrete, terracotta, wool, and even glass. These each come with their vertical counterpart. 

## Vertical Vanilla Slabs

Slabby finds all slabs in Minecraft and creates a vertical version of each one. Slabby copies all settings of each slab into the new vertical one and adds useful recipes so that the new slabs are all easily accessible. Want a horizontal slab to be a vertical one? Just open your inventory, and switch it in your 2x2 crafting grid. This is obviously reversible. All vertical stone and copper slab recipes have also been added to the stonecutter recipe book.

![](https://media.forgecdn.net/attachments/425/635/conversion-collage.png)

Placing vertical slabs can be tricky. Unlike the horizontal variant, the placement is based on the X **and** Z axis instead of just Y. 

To place a vertical slab in the orientation you want on the ground or ceiling, place it in the correct quadrant shown here:

![](https://media.forgecdn.net/attachments/425/634/quadrant-collage.png)

To place a vertical slab into the face of another block, imagine the block face in thirds, and place it on which side you want it to face. If you place in the center third it will place directly into the block face:

![](https://media.forgecdn.net/attachments/425/636/slices-collage.png)

![](https://media.forgecdn.net/attachments/425/637/slab-slices-collage.png)

## Showcase

![](https://media.forgecdn.net/attachments/425/605/2022-01-15_16.png)
![](https://media.forgecdn.net/attachments/425/606/2022-01-15_16.png)
![](https://media.forgecdn.net/attachments/425/603/2022-01-15_16.png)

## Vertical Modded Slabs

**Not implemented yet.**

Slabby was built in such a way that it will react to what it is given. This means that all future updates to Minecraft should only require changing the target minecraft version on the mod for it to work (unless they make something like Oxidizable again...). It also means that this should work with all modded slabs that inherit from SlabBlock. Anything that doesn't will not be made vertical by this mod.

### Known Bugs

* Vertical smooth stone slabs - They are currently just half of a full smooth stone block and don't have the ribbing like the double horizontal smooth stone slabs do. This will be worked on ASAP.

### Changelog

* Recipes are added to the recipe book upon receiving the necessary materials [0.2.3]
* Added recipes for combining slabs back into full blocks. [0.2.3]
* Bug Fixes: flammable slabs now burn, wool slabs are shearable [0.2.3]
