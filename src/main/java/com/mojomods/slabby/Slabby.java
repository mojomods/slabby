package com.mojomods.slabby;

import com.mojomods.slabby.resources.*;
import net.fabricmc.api.ModInitializer;

import static com.mojomods.slabby.util.SlabbyLogUtils.*;

public class Slabby implements ModInitializer {

    public static final String MOD_ID = "slabby";
    public static final String MOD_LABEL = MOD_ID.toUpperCase();

    public static final SlabbyDynamicResourcePack ASSETS_RESOURCE_PACK = new SlabbyDynamicResourcePack();
    public static final SlabbyDynamicResourcePack DATA_RESOURCE_PACK = new SlabbyDynamicResourcePack();

    private final SlabbyVanillaResourcesPuller resourcesPuller = new SlabbyVanillaResourcesPuller();
    private final SlabbyLangGenerator langGenerator = new SlabbyLangGenerator();
    private final SlabbyTagsGenerator tagsGenerator = new SlabbyTagsGenerator();
    private final SlabbyDecorationSlabsGenerator decorationSlabsGenerator
            = new SlabbyDecorationSlabsGenerator(resourcesPuller, langGenerator, tagsGenerator);
    private final SlabbyVerticalVanillaSlabsGenerator vanillaVerticalSlabsGenerator
            = new SlabbyVerticalVanillaSlabsGenerator(resourcesPuller, langGenerator, tagsGenerator);
    private final SlabbyVerticalModdedSlabsGenerator moddedVerticalSlabsGenerator = new SlabbyVerticalModdedSlabsGenerator();
    private final SlabbyOxidizableGenerator oxidizableGenerator = new SlabbyOxidizableGenerator();

    @Override
    public void onInitialize() {
        long start = System.currentTimeMillis();
        logDebug("Vertical Slab Generation Start");
        logDebug("==============================");
        resourcesPuller.pull();
        decorationSlabsGenerator.generate();
        vanillaVerticalSlabsGenerator.generate();
        moddedVerticalSlabsGenerator.generate();
        langGenerator.generate();
        tagsGenerator.generate();
        oxidizableGenerator.generate();
        logDebug("==============================");
        logDebug("Vertical Slab Generation Finish: " + (System.currentTimeMillis() - start) + "ms");
    }
}
