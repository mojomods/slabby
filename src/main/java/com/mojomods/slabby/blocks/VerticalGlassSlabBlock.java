package com.mojomods.slabby.blocks;

import com.mojomods.slabby.enums.VerticalSlabType;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;

public class VerticalGlassSlabBlock extends VerticalSlabBlock {

    public VerticalGlassSlabBlock(AbstractBlock.Settings settings, Identifier originBlock) {
        super(settings, originBlock);
    }

    // Overrides from AbstractGlassBlock

    @Override
    public VoxelShape getCameraCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return VoxelShapes.empty();
    }

    @Override
    public float getAmbientOcclusionLightLevel(BlockState state, BlockView world, BlockPos pos) {
        return 1.0f;
    }

    @Override
    public boolean isTranslucent(BlockState state, BlockView world, BlockPos pos) {
        return true;
    }

    // Overrides from TransparentBlock

    @Override
    public boolean isSideInvisible(BlockState state, BlockState stateFrom, Direction direction) {
        if (stateFrom.isOf(this) && stateFrom.get(TYPE) == VerticalSlabType.DOUBLE) {
            return true;
        }
        return super.isSideInvisible(state, stateFrom, direction);
    }
}
