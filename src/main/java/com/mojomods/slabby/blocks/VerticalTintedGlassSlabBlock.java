package com.mojomods.slabby.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;

public class VerticalTintedGlassSlabBlock extends VerticalGlassSlabBlock {
    public VerticalTintedGlassSlabBlock(Settings settings, Identifier originBlock) {
        super(settings, originBlock);
    }

    @Override
    public boolean isTranslucent(BlockState state, BlockView world, BlockPos pos) {
        return false;
    }

    @Override
    public int getOpacity(BlockState state, BlockView world, BlockPos pos) {
        return world.getMaxLightLevel();
    }
}
