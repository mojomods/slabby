package com.mojomods.slabby.blocks;

import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

import java.util.Locale;
import java.util.Optional;
import java.util.Random;

public class VerticalOxidizableSlabBlock extends VerticalSlabBlock implements Oxidizable {

    private final Oxidizable.OxidationLevel oxidizationLevel;

    /**
     * A vertical version of OxidizableSlabBlock. But obviously Oxidizable sucks so I'm rewriting everything.
     *
     * @param settings    : a copy of the horizontal version of the block settings
     * @param originBlock
     * @see Oxidizable
     * @see OxidizableSlabBlock
     * @see Block
     */
    public VerticalOxidizableSlabBlock(Settings settings, Identifier originBlock, Oxidizable.OxidationLevel oxidizationLevel) {
        super(settings, originBlock);
        this.oxidizationLevel = oxidizationLevel;
    }

    @Override
    public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        this.tickDegradation(state, world, pos, random);
    }

    @Override
    public boolean hasRandomTicks(BlockState state) {
        return this.oxidizationLevel != Oxidizable.OxidationLevel.OXIDIZED;
    }

    @Override
    public Oxidizable.OxidationLevel getDegradationLevel() {
        return this.oxidizationLevel;
    }
}
