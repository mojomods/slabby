package com.mojomods.slabby.blocks;

import com.mojomods.slabby.enums.VerticalSlabType;
import net.minecraft.block.*;
import net.minecraft.entity.ai.pathing.NavigationType;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.tag.FluidTags;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.WorldAccess;
import org.jetbrains.annotations.Nullable;

import static net.minecraft.entity.ai.pathing.NavigationType.WATER;

@SuppressWarnings("deprecation")
public class VerticalSlabBlock extends Block implements Waterloggable {
    public static final EnumProperty<VerticalSlabType> TYPE;
    public static final BooleanProperty WATERLOGGED;
    protected static final VoxelShape NORTH_SHAPE;
    protected static final VoxelShape EAST_SHAPE;
    protected static final VoxelShape SOUTH_SHAPE;
    protected static final VoxelShape WEST_SHAPE;

    public Identifier parentBlock;

    /**
     * A vertical version of SlabBlock. The main difference is the TYPE is based on X and Z values, not Y.
     * @param settings: a copy of the horizontal version of the block settings
     * @see SlabBlock
     * @see Block
     * @see Waterloggable
     */
    public VerticalSlabBlock(AbstractBlock.Settings settings, Identifier originBlock) {
        super(settings);
        this.parentBlock = originBlock;
        this.setDefaultState((this.getDefaultState().with(TYPE, VerticalSlabType.NORTH)).with(WATERLOGGED, false));
    }

    @Override
    public boolean hasSidedTransparency(BlockState state) {
        return state.get(TYPE) != VerticalSlabType.DOUBLE;
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(TYPE, WATERLOGGED);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        VerticalSlabType slabType = state.get(TYPE);
        return switch (slabType) {
            case DOUBLE -> VoxelShapes.fullCube();
            case EAST -> EAST_SHAPE;
            case SOUTH -> SOUTH_SHAPE;
            case WEST -> WEST_SHAPE;
            default -> NORTH_SHAPE;
        };
    }

    @Override
    @Nullable
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        BlockPos blockPos = ctx.getBlockPos();
        BlockState existingBlockState = ctx.getWorld().getBlockState(blockPos);
        if (existingBlockState.isOf(this)) {
            return existingBlockState.with(TYPE, VerticalSlabType.DOUBLE).with(WATERLOGGED, false);
        } else {
            BlockState newBlockState = getDefaultState().with(TYPE, VerticalSlabType.NORTH)
                    .with(WATERLOGGED, ctx.getWorld().getFluidState(blockPos).getFluid() == Fluids.WATER);

            return newBlockState.with(TYPE, switch (ctx.getSide()) {
                case NORTH, SOUTH, EAST, WEST -> getPlacementTypeBySideHitPos(ctx);
                case DOWN, UP -> getPlacementTypeByTopHitPos(ctx);
            });
        }
    }

    private VerticalSlabType getPlacementTypeBySideHitPos(ItemPlacementContext ctx) {
        var hitPos = ctx.getHitPos();
        var blockPos = ctx.getBlockPos();
        var x = hitPos.x - blockPos.getX();
        if (x < 0) x = blockPos.getX() - hitPos.x;
        var z = hitPos.z - blockPos.getZ();
        if (z < 0) z = blockPos.getZ() - hitPos.z;

        switch (ctx.getSide()) {
            case NORTH -> {
                if (x < (1F / 3F)) return VerticalSlabType.WEST;
                else if (x > (2F / 3F)) return VerticalSlabType.EAST;
                else return VerticalSlabType.SOUTH;
            }
            case SOUTH -> {
                if (x < (1F / 3F)) return VerticalSlabType.WEST;
                else if (x > (2F / 3F)) return VerticalSlabType.EAST;
                else return VerticalSlabType.NORTH;
            }
            case WEST -> {
                if (z < (1F / 3F)) return VerticalSlabType.NORTH;
                else if (z > (2F / 3F)) return VerticalSlabType.SOUTH;
                else return VerticalSlabType.EAST;
            }
            case EAST -> {
                if (z < (1F / 3F)) return VerticalSlabType.NORTH;
                else if (z > (2F / 3F)) return VerticalSlabType.SOUTH;
                else return VerticalSlabType.WEST;
            }
            default ->  {
                return switch (ctx.getPlayerFacing()) {
                    case NORTH -> VerticalSlabType.SOUTH;
                    case EAST -> VerticalSlabType.WEST;
                    case WEST -> VerticalSlabType.EAST;
                    default -> VerticalSlabType.NORTH;
                };
            }
        }
    }

    private VerticalSlabType getPlacementTypeByTopHitPos(ItemPlacementContext ctx) {
        var hitPos = ctx.getHitPos();
        var blockPos = ctx.getBlockPos();
        var x = hitPos.x - blockPos.getX();
        if (x < 0) x = blockPos.getX() - hitPos.x;
        var z = hitPos.z - blockPos.getZ();
        if (z < 0) z = blockPos.getZ() - hitPos.z;
        if (x == .5 && z == .5) {
            return switch (ctx.getPlayerFacing()) {
                case SOUTH -> VerticalSlabType.SOUTH;
                case EAST -> VerticalSlabType.EAST;
                case WEST -> VerticalSlabType.WEST;
                default -> VerticalSlabType.NORTH;
            };
        } else if (x == 0 && z == 0) {
            return VerticalSlabType.SOUTH;
        } else if (x == 0 && z == 1) {
            return VerticalSlabType.WEST;
        } else if (x == 1 && z == 0) {
            return VerticalSlabType.EAST;
        } else {
            boolean aboveA = z > (1 - x);
            boolean aboveB = z > x;
            if (aboveA && aboveB) {
                return VerticalSlabType.SOUTH;
            } else if (aboveA) {
                return VerticalSlabType.EAST;
            } else if (aboveB) {
                return VerticalSlabType.WEST;
            } else {
                return VerticalSlabType.NORTH;
            }
        }
    }

    @Override
    public boolean canReplace(BlockState state, ItemPlacementContext context) {
        VerticalSlabType slabType = state.get(TYPE);
        if (slabType != VerticalSlabType.DOUBLE && context.getStack().isOf(asItem())) {
            if (context.canReplaceExisting()) {
                boolean blz = context.getHitPos().z - (double)context.getBlockPos().getZ() > 0.5;
                boolean blx = context.getHitPos().x - (double)context.getBlockPos().getX() > 0.5;
                Direction direction = context.getSide();
                return switch (slabType) {
                    case NORTH -> direction == Direction.SOUTH || blz && direction.getAxis().isVertical();
                    case SOUTH -> direction == Direction.NORTH || !blz && direction.getAxis().isVertical();
                    case EAST -> direction == Direction.WEST || !blx && direction.getAxis().isVertical();
                    default -> direction == Direction.EAST || blx && direction.getAxis().isVertical();
                };
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(WATERLOGGED) ? Fluids.WATER.getStill(false) : super.getFluidState(state);
    }

    @Override
    public boolean tryFillWithFluid(WorldAccess world, BlockPos pos, BlockState state, FluidState fluidState) {
        return state.get(TYPE) != VerticalSlabType.DOUBLE && Waterloggable.super.tryFillWithFluid(world, pos, state, fluidState);
    }

    @Override
    public boolean canFillWithFluid(BlockView world, BlockPos pos, BlockState state, Fluid fluid) {
        return state.get(TYPE) != VerticalSlabType.DOUBLE && Waterloggable.super.canFillWithFluid(world, pos, state, fluid);
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState, WorldAccess world, BlockPos pos, BlockPos neighborPos) {
        if (state.get(WATERLOGGED)) {
            world.createAndScheduleFluidTick(pos, Fluids.WATER, Fluids.WATER.getTickRate(world));
        }

        return super.getStateForNeighborUpdate(state, direction, neighborState, world, pos, neighborPos);
    }

    @Override
    public boolean canPathfindThrough(BlockState state, BlockView world, BlockPos pos, NavigationType type) {
        if (type == WATER) {
            return world.getFluidState(pos).isIn(FluidTags.WATER);
        } else return false;
    }



    static {
        TYPE = VerticalSlabType.getEnumProperty();
        WATERLOGGED = Properties.WATERLOGGED;
        NORTH_SHAPE = createCuboidShape(0.0, 0.0, 0.0, 16.0, 16.0, 8.0);
        EAST_SHAPE = createCuboidShape(8.0, 0.0, 0.0, 16.0, 16.0, 16.0);
        SOUTH_SHAPE = createCuboidShape(0.0, 0.0, 8.0, 16.0, 16.0, 16.0);
        WEST_SHAPE = createCuboidShape(0.0, 0.0, 0.0, 8.0, 16.0, 16.0);
    }
}
