package com.mojomods.slabby.blocks;

import net.minecraft.block.Stainable;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;

public class VerticalStainedGlassSlabBlock extends VerticalGlassSlabBlock implements Stainable {
    private final DyeColor color;

    public VerticalStainedGlassSlabBlock(DyeColor color, Settings settings, Identifier originBlock) {
        super(settings, originBlock);
        this.color = color;
    }

    @Override
    public DyeColor getColor() {
        return this.color;
    }
}
