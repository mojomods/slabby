package com.mojomods.slabby.blocks;

import net.minecraft.block.*;
import net.minecraft.util.DyeColor;

public class StainedGlassSlabBlock extends GlassSlabBlock implements Stainable {
    private final DyeColor color;

    public StainedGlassSlabBlock(DyeColor color, Settings settings) {
        super(settings);
        this.color = color;
    }

    @Override
    public DyeColor getColor() {
        return this.color;
    }
}
