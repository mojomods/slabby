package com.mojomods.slabby.resources;

import com.google.gson.*;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.mojomods.slabby.util.SlabbyLogUtils.*;
import static com.mojomods.slabby.util.SlabbyStringUtils.*;

public class SlabbyVanillaResourcesPuller {
    private final List<String> pickaxeMineable = new ArrayList<>();
    private final List<String> axeMineable = new ArrayList<>();
    private final List<String> shovelMineable = new ArrayList<>();
    private final List<String> hoeMineable = new ArrayList<>();

    private final List<String> wool = new ArrayList<>();

    private final List<String> needsStoneTool = new ArrayList<>();
    private final List<String> needsIronTool = new ArrayList<>();
    private final List<String> needsDiamondTool = new ArrayList<>();

    private final Map<String, String> slabRecipes = new HashMap<>();
    private final Map<String, String> stonecuttingRecipes = new HashMap<>();
    private final Map<String, String> slabMiscRecipes = new HashMap<>();

    private final Map<String, String> slabRecipeTriggers = new HashMap<>();

    private final Map<String, String> lootTables = new HashMap<>();

    private ZipFile root;

    public void pull() {
        logDebug("Resources Puller - Start");
        var start = System.currentTimeMillis();

        try {
            URL rootUrl = DefaultResourcePack.class.getResource("/data/.mcassetsroot");
            if (rootUrl != null) {
                JarURLConnection connection = (JarURLConnection) rootUrl.openConnection();
                root = new ZipFile(Paths.get(connection.getJarFileURL().toURI()).toFile());
            } else throw new Exception();
        } catch (Exception e) {
            logDebug("Resources Puller - Failure");
            logError("Unable to locate Minecraft Jar");
            e.printStackTrace();
            return;
        }

        String pickaxeFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/mineable/pickaxe.json"));
        String axeFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/mineable/axe.json"));
        String shovelFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/mineable/shovel.json"));
        String hoeFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/mineable/hoe.json"));
        String stoneFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/needs_stone_tool.json"));
        String ironFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/needs_iron_tool.json"));
        String diamondFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/needs_diamond_tool.json"));
        String woolFile = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:tags/blocks/wool.json"));

        try {
            String pickaxeJson = getFileJson(pickaxeFile);
            String axeJson = getFileJson(axeFile);
            String shovelJson = getFileJson(shovelFile);
            String hoeJson = getFileJson(hoeFile);
            String stoneJson = getFileJson(stoneFile);
            String ironJson = getFileJson(ironFile);
            String diamondJson = getFileJson(diamondFile);
            String woolJson = getFileJson(woolFile);

            if (pickaxeJson != null) {
                JsonArray pickaxeJsonArr = JsonParser.parseString(pickaxeJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(pickaxeJsonArr, pickaxeMineable);
            }
            if (axeJson != null) {
                JsonArray axeJsonArr = JsonParser.parseString(axeJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(axeJsonArr, axeMineable);
            }
            if (shovelJson != null) {
                JsonArray shovelJsonArr = JsonParser.parseString(shovelJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(shovelJsonArr, shovelMineable);
            }
            if (hoeJson != null) {
                JsonArray hoeJsonArr = JsonParser.parseString(hoeJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(hoeJsonArr, hoeMineable);
            }
            if (stoneJson != null) {
                JsonArray stoneJsonArr = JsonParser.parseString(stoneJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(stoneJsonArr, needsStoneTool);
            }
            if (ironJson != null) {
                JsonArray ironJsonArr = JsonParser.parseString(ironJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(ironJsonArr, needsIronTool);
            }
            if (diamondJson != null) {
                JsonArray diamondJsonArr = JsonParser.parseString(diamondJson).getAsJsonObject().get("values").getAsJsonArray();
                packageMineableList(diamondJsonArr, needsDiamondTool);
            }
            if (woolJson != null) {
                JsonArray diamondJsonArr = JsonParser.parseString(woolJson).getAsJsonObject().get("values").getAsJsonArray();
                for (JsonElement e : diamondJsonArr) { wool.add(e.getAsString()); }
            }

            String recipesFolder = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:recipes"));
            String recipeTriggersFolder = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:advancements/recipes/building_blocks"));
            String lootTablesFolder = massageFilePath(ResourceType.SERVER_DATA, new Identifier("minecraft:loot_tables/blocks"));

            root.stream()
                    .filter(entry -> entry.getName().startsWith(ResourceType.SERVER_DATA.getDirectory() + "/minecraft"))
                    .forEach(entry -> {
                        try {
                            String fileName = entry.getName();
                            if (fileName.startsWith(recipesFolder) && fileName.contains("_slab")) {
                                String recipeJson = getFileJson(entry);
                                if (fileName.contains("_stonecutting")) {
                                    addStonecuttingRecipe(getFileNameFromPath(fileName), recipeJson);
                                } else if (fileName.endsWith("_slab.json")) {
                                    addSlabRecipe(getFileNameFromPath(fileName), recipeJson);
                                } else {
                                    addMiscRecipe(getFileNameFromPath(fileName), recipeJson);
                                }
                            } else if (fileName.startsWith(recipeTriggersFolder) && fileName.contains("_slab")) {
                                String triggerJson = getFileJson(entry);
                                addSlabRecipeTrigger(getFileNameFromPath(fileName), triggerJson);
                            } else if (fileName.startsWith(lootTablesFolder) && fileName.contains("_slab")) {
                                String lootTableJson = getFileJson(entry);
                                addLootTable(fileName, lootTableJson);
                            }
                        } catch (IOException e) {
                            logError("Unable to read " + entry.getName());
                        }
                    });
            logDebug("Resources Puller - Success: " + (System.currentTimeMillis() - start) + "ms");
        } catch (IOException e) {
            logDebug("Resources Puller - Failure");
            logError(e.getMessage());
            e.printStackTrace();
        }
    }

    @Nullable
    private String getFileJson(String fileName) throws IOException {
        ZipEntry zipEntry = root.getEntry(fileName);
        if (zipEntry != null) {
            return getFileJson(zipEntry);
        }
        return null;
    }

    private String getFileJson(ZipEntry zipEntry) throws IOException {
        return new String(root.getInputStream(zipEntry).readAllBytes(), StandardCharsets.UTF_8);
    }

    private void packageMineableList(JsonArray arr, List<String> list) throws IOException {
        for (JsonElement jsonElement : arr) {
            String block = jsonElement.getAsString();
            if (block.contains("#")) {
                String tagFile = massageFilePath(ResourceType.SERVER_DATA,
                        new Identifier("minecraft:tags/blocks/" + block.split(":")[1] + ".json"));
                String tagJson = getFileJson(tagFile);
                if (tagJson != null) {
                    for (JsonElement subBlock : JsonParser.parseString(tagJson).getAsJsonObject().get("values").getAsJsonArray()) {
                        list.add(subBlock.getAsString());
                    }
                }
            } else {
                list.add(block);
            }
        }
    }

    public void addNeedsStoneTool(String blockID) { this.needsStoneTool.add(blockID); }
    public void addNeedsIronTool(String blockID) { this.needsIronTool.add(blockID); }
    public void addNeedsDiamondTool(String blockID) { this.needsDiamondTool.add(blockID); }

    public boolean needsStoneTool(String blockID) { return this.needsStoneTool.contains(blockID); }
    public boolean needsIronTool(String blockID) { return this.needsIronTool.contains(blockID); }
    public boolean needsDiamondTool(String blockID) { return this.needsDiamondTool.contains(blockID); }

    public void addPickaxeMineable(String blockID) { this.pickaxeMineable.add(blockID); }
    public void addAxeMineable(String blockID) { this.axeMineable.add(blockID); }
    public void addShovelMineable(String blockID) { this.shovelMineable.add(blockID); }
    public void addHoeMineable(String blockID) { this.hoeMineable.add(blockID); }

    public void addWool(String blockID) { this.wool.add(blockID); }

    public boolean pickaxeMineable(String blockID) { return this.pickaxeMineable.contains(blockID); }
    public boolean axeMineable(String blockID) { return this.axeMineable.contains(blockID); }
    public boolean shovelMineable(String blockID) { return this.shovelMineable.contains(blockID); }
    public boolean hoeMineable(String blockID) { return this.hoeMineable.contains(blockID); }

    public boolean isWool(String blockID) { return this.wool.contains(blockID); }

    public void addSlabRecipe(String fileName, String recipe) { this.slabRecipes.put(fileName, recipe); }
    public void addStonecuttingRecipe(String fileName, String recipe) { this.stonecuttingRecipes.put(fileName, recipe); }
    public void addMiscRecipe(String fileName, String recipe) { this.slabMiscRecipes.put(fileName, recipe); }

    public void addSlabRecipeTrigger(String fileName, String recipe) { this.slabRecipeTriggers.put(fileName, recipe); }

    public void addLootTable(String fileName, String lootTable) { this.lootTables.put(fileName, lootTable); }

    public Map<String, String> getSlabRecipes() { return this.slabRecipes; }
    public Map<String, String> getSlabStonecuttingRecipes() { return this.stonecuttingRecipes; }
    public Map<String, String> getMiscSlabRecipes() { return this.slabMiscRecipes; }

    public Map<String, String> getSlabRecipeTriggers() { return this.slabRecipeTriggers; }

    public Map<String, String> getLootTables() { return this.lootTables; }

}
