package com.mojomods.slabby.resources;

import com.google.gson.Gson;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

import static com.mojomods.slabby.Slabby.*;
import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;
import static com.mojomods.slabby.util.SlabbyStringUtils.*;

public class SlabbyTagsGenerator {

    private final Gson GSON = new Gson();

    private final List<String> needsStoneTool = new ArrayList<>();
    private final List<String> needsIronTool = new ArrayList<>();
    private final List<String> needsDiamondTool = new ArrayList<>();

    private final List<String> pickaxeMineable = new ArrayList<>();
    private final List<String> axeMineable = new ArrayList<>();
    private final List<String> shovelMineable = new ArrayList<>();
    private final List<String> hoeMineable = new ArrayList<>();

    private final List<String> wool = new ArrayList<>();

    public void generate() {
        logDebug("Tags Generator - Start");
        var start = System.currentTimeMillis();

        String needsStoneToolJson = tagJson(needsStoneTool);
        String needsIronToolJson = tagJson(needsIronTool);
        String needsDiamondToolJson = tagJson(needsDiamondTool);

        String pickaxeMineableJson = tagJson(pickaxeMineable);
        String axeMineableJson = tagJson(axeMineable);
        String shovelMineableJson = tagJson(shovelMineable);
        String hoeMineableJson = tagJson(hoeMineable);

        String woolTagJson = tagJson(wool);

        // Needs Stone Tool
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "needs_stone_tool"), "tags/blocks"),
                toByteArray(needsStoneToolJson)
        );
        // Needs Iron Tool
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "needs_iron_tool"), "tags/blocks"),
                toByteArray(needsIronToolJson)
        );
        // Needs Stone Tool
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "needs_diamond_tool"), "tags/blocks"),
                toByteArray(needsDiamondToolJson)
        );

        // Pickaxe Mineable
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "pickaxe"), "tags/blocks/mineable"),
                toByteArray(pickaxeMineableJson)
        );
        // Axe Mineable
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "axe"), "tags/blocks/mineable"),
                toByteArray(axeMineableJson)
        );
        // Shovel Mineable
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "shovel"), "tags/blocks/mineable"),
                toByteArray(shovelMineableJson)
        );
        // Hoe Mineable
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "hoe"), "tags/blocks/mineable"),
                toByteArray(hoeMineableJson)
        );

        // Wool Tags (for shears mineable)
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.TAG,
                massageIdentifierPath(new Identifier("minecraft", "wool"), "tags/blocks"),
                toByteArray(woolTagJson)
        );


        logDebug("Tags Generator - Success: " + (System.currentTimeMillis() - start) + "ms");
    }

    private String tagJson(List<String> idList) {
        return "{ \"replace\": false, \"values\": " + GSON.toJson(idList) + " }";
    }

    public void addNeedsStoneTool(Identifier id) { needsStoneTool.add(id.toString()); }
    public void addNeedsIronTool(Identifier id) { needsIronTool.add(id.toString()); }
    public void addNeedsDiamondTool(Identifier id) { needsDiamondTool.add(id.toString()); }

    public void addPickaxeMineable(Identifier id) { pickaxeMineable.add(id.toString()); }
    public void addAxeMineable(Identifier id) { axeMineable.add(id.toString()); }
    public void addShovelMineable(Identifier id) { shovelMineable.add(id.toString()); }
    public void addHoeMineable(Identifier id) { hoeMineable.add(id.toString()); }

    public void addWool(Identifier id) { wool.add(id.toString()); }
}
