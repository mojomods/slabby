package com.mojomods.slabby.resources;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojomods.slabby.Slabby;
import com.mojomods.slabby.models.SlabbyBlockTextures;
import net.minecraft.resource.DefaultResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.mojomods.slabby.util.SlabbyStringUtils.*;

public final class SlabbyTexturesPuller {
    public static SlabbyBlockTextures pullSlabTextures(Identifier originBlockID) throws IOException {
        Identifier originBlockFileID = new Identifier("minecraft:models/block/" + getBlockPathFromIDForTextures(originBlockID) + ".json");
        String originBlockFile = massageResourcePath(ResourceType.CLIENT_RESOURCES, originBlockFileID);
        URL originUrl = DefaultResourcePack.class.getResource(originBlockFile);
        if (originUrl != null) {
            JsonObject texturesJson = JsonParser.parseReader(new InputStreamReader(originUrl.openStream()))
                    .getAsJsonObject().get("textures").getAsJsonObject();
            return SlabbyBlockTextures.fromSlab(texturesJson, getBlockPathFromID(originBlockID));
        } else if (Slabby.ASSETS_RESOURCE_PACK.contains(ResourceType.CLIENT_RESOURCES, originBlockFileID)) {
            var data = Slabby.ASSETS_RESOURCE_PACK.getResource(ResourceType.CLIENT_RESOURCES, originBlockFileID);
            JsonObject texturesJson = JsonParser.parseString(new String(data, StandardCharsets.UTF_8))
                    .getAsJsonObject().get("textures").getAsJsonObject();
            return SlabbyBlockTextures.fromSlab(texturesJson, getBlockPathFromID(originBlockID));
        }
        throw new IOException();
    }

    public static String pullBlockTexture(Identifier originBlockID) throws IOException {
        String originBlockFile = massageResourcePath(ResourceType.CLIENT_RESOURCES,
                new Identifier("minecraft:models/block/" + getBlockPathFromIDForTextures(originBlockID) + ".json"));
        URL originUrl = DefaultResourcePack.class.getResource(originBlockFile);
        if (originUrl != null) {
            JsonObject texturesJson = JsonParser.parseReader(new InputStreamReader(originUrl.openStream()))
                    .getAsJsonObject().get("textures").getAsJsonObject();
            return texturesJson.get("all").getAsString();
        }
        throw new IOException();
    }
}
