package com.mojomods.slabby.resources;

import com.google.gson.*;
import com.mojomods.slabby.blocks.*;
import com.mojomods.slabby.models.SlabbyBlockTextures;
import com.mojomods.slabby.util.SlabbyResourceUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.registry.FlammableBlockRegistry;
import net.fabricmc.fabric.impl.content.registry.FlammableBlockRegistryImpl;
import net.fabricmc.fabric.mixin.object.builder.AbstractBlockSettingsAccessor;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Oxidizable;
import net.minecraft.block.SlabBlock;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.mojomods.slabby.Slabby.ASSETS_RESOURCE_PACK;
import static com.mojomods.slabby.Slabby.DATA_RESOURCE_PACK;
import static com.mojomods.slabby.Slabby.MOD_ID;
import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;
import static com.mojomods.slabby.util.SlabbyLogUtils.logError;
import static com.mojomods.slabby.util.SlabbyStringUtils.*;
import static com.mojomods.slabby.util.SlabbyStringUtils.massageIdentifierPath;

public class SlabbyVerticalVanillaSlabsGenerator {

    private final Gson GSON = new Gson();

    private final SlabbyVanillaResourcesPuller resourcesPuller;
    private final SlabbyLangGenerator langGenerator;
    private final SlabbyTagsGenerator tagsGenerator;

    private final FlammableBlockRegistryImpl flammableBlockRegistry = (FlammableBlockRegistryImpl) FlammableBlockRegistry.getDefaultInstance();

    public SlabbyVerticalVanillaSlabsGenerator(
            SlabbyVanillaResourcesPuller tagsAndRecipesGenerator,
            SlabbyLangGenerator langGenerator,
            SlabbyTagsGenerator tagsGenerator
    ) {
        this.resourcesPuller = tagsAndRecipesGenerator;
        this.langGenerator = langGenerator;
        this.tagsGenerator = tagsGenerator;
    }

    public void generate() {
        logDebug("Vanilla Vertical Slabs Generator - Start");
        var start = System.currentTimeMillis();
        flammableBlockRegistry.reload(null);
        Registry.ITEM.stream()
                .filter(item -> item instanceof BlockItem)
                .forEach(item -> createVerticalVanillaSlabs((BlockItem) item));
        createNormalRecipes();
        createStonecuttingRecipes();
        createOtherRecipes();
        createSlabRecipeTriggers();
        createLootTableResource();
        logDebug("Vanilla Vertical Slabs Generator - Success: " + (System.currentTimeMillis() - start) + "ms");
    }

    private void createVerticalVanillaSlabs(BlockItem originItem) {
        SlabBlock originBlock;
        try {
            originBlock = (SlabBlock) originItem.getBlock();
        } catch (Exception e) {
            return;
        }

        Identifier originBlockID = Registry.BLOCK.getId(originBlock);
        String namespace = originBlockID.getNamespace();
        String originName = originBlockID.getPath();

        if (!namespace.equals("minecraft")) return; // this section is only for vanilla slabs

        String targetBlockName = "vertical_" + originName;
        Identifier targetBlockID = new Identifier(namespace, targetBlockName);

        if (Registry.BLOCK.containsId(targetBlockID)) return; // can't double up on blocks

        // Item Settings
        FabricItemSettings targetItemSettings = new FabricItemSettings();
        targetItemSettings.maxDamage(originItem.getMaxDamage());
        targetItemSettings.maxCount(originItem.getMaxCount());
        targetItemSettings.recipeRemainder(originItem.getRecipeRemainder());
        targetItemSettings.group(originItem.getGroup());
        targetItemSettings.rarity(originItem.getRarity(originItem.getDefaultStack()));
        targetItemSettings.food(originItem.getFoodComponent());
        if (originItem.isFireproof()) targetItemSettings.fireproof();

        // Block Settings
        FabricBlockSettings targetBlockSettings = FabricBlockSettings.copyOf(originBlock);

        VerticalSlabBlock targetBlock;
        if (originBlock instanceof Oxidizable) {
            targetBlock = new VerticalOxidizableSlabBlock(
                    targetBlockSettings,
                    originBlockID,
                    ((Oxidizable) originBlock).getDegradationLevel());
        } else if (originBlock instanceof StainedGlassSlabBlock) {
            targetBlock = new VerticalStainedGlassSlabBlock(
                    ((StainedGlassSlabBlock) originBlock).getColor(),
                    targetBlockSettings.blockVision(SlabbyResourceUtils::never)
                            .allowsSpawning(SlabbyResourceUtils::never)
                            .solidBlock(SlabbyResourceUtils::never)
                            .suffocates(SlabbyResourceUtils::never),
                    originBlockID);
        } else if (originBlock instanceof TintedGlassSlabBlock) {
            targetBlock = new VerticalTintedGlassSlabBlock(
                    targetBlockSettings.blockVision(SlabbyResourceUtils::never)
                            .allowsSpawning(SlabbyResourceUtils::never)
                            .solidBlock(SlabbyResourceUtils::never)
                            .suffocates(SlabbyResourceUtils::never),
                    originBlockID);
        } else if (originBlock instanceof GlassSlabBlock) {
            targetBlock = new VerticalGlassSlabBlock(
                    targetBlockSettings.blockVision(SlabbyResourceUtils::never)
                            .allowsSpawning(SlabbyResourceUtils::never)
                            .solidBlock(SlabbyResourceUtils::never)
                            .suffocates(SlabbyResourceUtils::never),
                    originBlockID);
        } else {
            targetBlock = new VerticalSlabBlock(targetBlockSettings, originBlockID);
        }

        if (((AbstractBlockSettingsAccessor) targetBlockSettings).getMaterial().isBurnable()) {

            var originFlammable = flammableBlockRegistry.get(originBlock);
            flammableBlockRegistry.add(targetBlock, originFlammable.getBurnChance(), originFlammable.getSpreadChance());
        }

        try {
            if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) {
                SlabbyBlockTextures textures = SlabbyTexturesPuller.pullSlabTextures(originBlockID);
                createVerticalSlabClientResources(targetBlockID, textures);
                if (targetBlock instanceof VerticalStainedGlassSlabBlock || targetBlock instanceof VerticalTintedGlassSlabBlock)
                    BlockRenderLayerMap.INSTANCE.putBlock(targetBlock, RenderLayer.getTranslucent());
                else if (targetBlock instanceof VerticalGlassSlabBlock)
                    BlockRenderLayerMap.INSTANCE.putBlock(targetBlock, RenderLayer.getCutout());
            }
        } catch (IOException e) {
            logError("Unable to pull block textures for " + originBlockID);
            logError(e.getMessage());
        }

        createVerticalSlabServerResources(originBlockID, targetBlockID);

        Registry.register(Registry.BLOCK, targetBlockID, targetBlock);
        Registry.register(Registry.ITEM, targetBlockID, new BlockItem(targetBlock, targetItemSettings));
    }

    @Environment(EnvType.CLIENT)
    private void createVerticalSlabClientResources(@NotNull Identifier targetBlockID, @NotNull SlabbyBlockTextures textures) {
        String namespace = targetBlockID.getNamespace();
        String targetName = targetBlockID.getPath();
        String targetBlockPathName = "block/" + targetName;
        String targetItemPathName = "item/" + targetName;

        createBlockModelResource(textures, namespace, targetBlockPathName);
        createItemModelResource(namespace, targetBlockPathName, targetItemPathName);
        createBlockStateResources(namespace, targetBlockID, targetBlockPathName, textures);

        /*  ==========          LANG        ==========
            "item.minecraft.vertical_stone_slab": "Vertical Stone Slab",
            "block.minecraft.vertical_stone_slab": "Vertical Stone Slab"
         */
        langGenerator.addLang(targetBlockID);
    }

    private void createVerticalSlabServerResources(@NotNull Identifier originBlockID, @NotNull Identifier targetBlockID) {
        createConversionRecipes(originBlockID, targetBlockID);
        createToolTagsResources(originBlockID, targetBlockID);
    }

    private void createBlockModelResource(
            @NotNull SlabbyBlockTextures textures,
            @NotNull String namespace,
            @NotNull String targetBlockPathName
    ) {
        /*  ==========      BLOCK MODEL     ==========
        {
          "parent": "slabby:block/vertical_north_slab",
          "textures": {
            "bottom": "minecraft:block/stone",
            "top": "minecraft:block/stone",
            "side": "minecraft:block/stone"
          }
        }
         */
        Arrays.stream(new String[] {"north", "east", "south", "west"}).forEach(dir -> {
            String blockJson = "{\n" +
                    "          \"parent\": \"" + MOD_ID + ":block/vertical_" + dir + "_slab\",\n" +
                    "          \"textures\": {\n" +
                    "            \"top\": \"" + textures.top + "\",\n" +
                    "            \"side\": \"" + textures.side + "\",\n" +
                    "            \"bottom\": \"" + textures.bottom + "\"\n" +
                    "          }\n" +
                    "        }";
            String path = dir.equals("north") ? targetBlockPathName : targetBlockPathName + "_" + dir;
            ASSETS_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.MODEL,
                    massageIdentifierPath(new Identifier(namespace, path), "models"),
                    toByteArray(blockJson)
            );
        });
    }

    private void createItemModelResource(
            @NotNull String namespace,
            @NotNull String targetBlockPathName,
            @NotNull String targetItemPathName
    ) {
        /*  ==========      ITEM MODEL     ==========
        {
          "parent": "block/vertical_stone_slab"
        }
         */
        String itemJson = "{ \"parent\": \"" + targetBlockPathName + "\" }";
        ASSETS_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.MODEL,
                massageIdentifierPath(new Identifier(namespace, targetItemPathName), "models"),
                toByteArray(itemJson)
        );
    }

    private void createBlockStateResources(
            @NotNull String namespace,
            @NotNull Identifier targetBlockID,
            @NotNull String targetBlockPathName,
            @NotNull SlabbyBlockTextures textures
    ) {
        /*  ==========      BLOCK STATE     ==========
        {
          "variants": {
            "type=west": {
              "model": "minecraft:block/vertical_stone_slab_west"
            },
            "type=east": {
              "model": "minecraft:block/vertical_stone_slab_east"
            },
            "type=north": {
              "model": "minecraft:block/vertical_stone_slab"
            },
            "type=south": {
              "model": "minecraft:block/vertical_stone_slab_south"
            },
            "type=double": {
              "model": "minecraft:block/stone"
            }
          }
        }
         */
        String blockState = "{\n" +
                "          \"variants\": {\n" +
                "            \"type=north\": {\n" +
                "              \"model\": \"" + namespace + ":" + targetBlockPathName + "\"\n" +
                "            },\n" +
                "            \"type=east\": {\n" +
                "              \"model\": \"" + namespace + ":" + targetBlockPathName + "_east\"\n" +
                "            },\n" +
                "            \"type=south\": {\n" +
                "              \"model\": \"" + namespace + ":" + targetBlockPathName + "_south\"\n" +
                "            },\n" +
                "            \"type=west\": {\n" +
                "              \"model\": \"" + namespace + ":" + targetBlockPathName + "_west\"\n" +
                "            },\n" +
                "            \"type=double\": {\n" +
                "              \"model\": \"" + textures.full + "\"\n" +
                "            }\n" +
                "          }\n" +
                "        }";
        ASSETS_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.BLOCK_STATE,
                massageIdentifierPath(targetBlockID, "blockstates"),
                toByteArray(blockState)
        );
    }

    private void createConversionRecipes(@NotNull Identifier originBlockID, @NotNull Identifier targetBlockID) {
        /*  ==========  FROM SLAB   ==========
        {
          "type": "minecraft:crafting_shapeless",
          "group": "minecraft:vertical_stone_slab",
          "ingredients": [
            {
              "item": "minecraft:stone_slab"
            }
          ],
          "result": {
            "item": "minecraft:vertical_stone_slab"
          }
        }
         */
        String fromSlabJson = "{\n" +
                "  \"type\": \"minecraft:crafting_shapeless\",\n" +
                "  \"group\": \"" + targetBlockID + "\",\n" +
                "  \"ingredients\": [\n" +
                "    {\n" +
                "      \"item\": \"" + originBlockID + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"result\": {\n" +
                "    \"item\": \"" + targetBlockID + "\"\n" +
                "  }\n" +
                "}";
        Identifier fromSlabRecipeID = new Identifier(targetBlockID.getNamespace(), targetBlockID.getPath() + "_from_" + originBlockID.getPath());
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                massageIdentifierPath(fromSlabRecipeID, "recipes"),
                toByteArray(fromSlabJson)
        );

        /*  ==========  TO SLAB   ==========
        {
          "type": "minecraft:crafting_shapeless",
          "group": "minecraft:stone_slab",
          "ingredients": [
            {
              "item": "minecraft:vertical_stone_slab"
            }
          ],
          "result": {
            "item": "minecraft:stone_slab"
          }
        }
         */
        String toSlabJson = "{\n" +
                "  \"type\": \"minecraft:crafting_shapeless\",\n" +
                "  \"group\": \"" + originBlockID + "\",\n" +
                "  \"ingredients\": [\n" +
                "    {\n" +
                "      \"item\": \"" + targetBlockID + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"result\": {\n" +
                "    \"item\": \"" + originBlockID + "\"\n" +
                "  }\n" +
                "}";
        Identifier toSlabRecipeID = new Identifier(originBlockID.getNamespace(), originBlockID.getPath() + "_from_" + targetBlockID.getPath());
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                massageIdentifierPath(toSlabRecipeID, "recipes"),
                toByteArray(toSlabJson)
        );

        /*  ==========  RECIPE TRIGGERS ==========
        {
          "parent": "minecraft:recipes/root",
          "rewards": {
            "recipes": [
              "minecraft:stone_slab_from_vertical_stone_slab",
              "minecraft:vertical_stone_slab_from_stone_slab"
            ]
          },
          "criteria": {
            "has_slab": {
              "trigger": "minecraft:recipe_unlocked",
              "conditions": {
                "recipe": "minecraft:stone_slab"
              }
            },
            "has_vertical_slab": {
              "trigger": "minecraft:recipe_unlocked",
              "conditions": {
                "recipe": "minecraft:vertical_stone_slab"
              }
            }
          },
          "requirements": [
            [
              "has_slab_recipe",
              "has_vertical_slab_recipe"
            ]
          ]
        }
        */
        String recipeTriggerJson = "{\n" +
                "          \"parent\": \"minecraft:recipes/root\",\n" +
                "          \"rewards\": {\n" +
                "            \"recipes\": [\n" +
                "              \"" + fromSlabRecipeID + "\",\n" +
                "              \"" + toSlabRecipeID + "\"\n" +
                "            ]\n" +
                "          },\n" +
                "          \"criteria\": {\n" +
                "            \"has_slab_recipe\": {\n" +
                "              \"trigger\": \"minecraft:recipe_unlocked\",\n" +
                "              \"conditions\": {\n" +
                "                \"recipe\": \"" + originBlockID + "\"\n" +
                "              }\n" +
                "            },\n" +
                "            \"has_vertical_slab_recipe\": {\n" +
                "              \"trigger\": \"minecraft:recipe_unlocked\",\n" +
                "              \"conditions\": {\n" +
                "                \"recipe\": \"" + targetBlockID+ "\"\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"requirements\": [\n" +
                "            [\n" +
                "              \"has_slab_recipe\",\n" +
                "              \"has_vertical_slab_recipe\"\n" +
                "            ]\n" +
                "          ]\n" +
                "        }";
        Identifier conversionRecipeID = new Identifier(originBlockID.getNamespace(), originBlockID.getPath() + "_conversions");
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE_TRIGGER,
                massageIdentifierPath(conversionRecipeID, "advancements/recipes/building_blocks"),
                toByteArray(recipeTriggerJson)
        );
    }

    private void createToolTagsResources(@NotNull Identifier originBlockID, @NotNull Identifier targetBlockID) {
        String originID = originBlockID.toString();
        if (resourcesPuller.needsStoneTool(originID))
            tagsGenerator.addNeedsStoneTool(targetBlockID);
        else if (resourcesPuller.needsIronTool(originID))
            tagsGenerator.addNeedsIronTool(targetBlockID);
        else if (resourcesPuller.needsDiamondTool(originID))
            tagsGenerator.addNeedsDiamondTool(targetBlockID);

        if (resourcesPuller.pickaxeMineable(originID))
            tagsGenerator.addPickaxeMineable(targetBlockID);
        else if (resourcesPuller.axeMineable(originID))
            tagsGenerator.addAxeMineable(targetBlockID);
        else if (resourcesPuller.shovelMineable(originID))
            tagsGenerator.addShovelMineable(targetBlockID);
        else if (resourcesPuller.hoeMineable(originID))
            tagsGenerator.addHoeMineable(targetBlockID);

        if (resourcesPuller.isWool(originID))
            tagsGenerator.addWool(targetBlockID);
    }

    private void createNormalRecipes() {
        /*  ==========  NORMAL RECIPE   ==========
        {
          "type": "minecraft:crafting_shaped",
          "group": "minecraft:stone_slab",
          "pattern": [
            "#",
            "#",
            "#"
          ],
          "key": {
            "#": {
              "item": "minecraft:stone"
            }
          },
          "result": {
            "item": "minecraft:vertical_stone_slab",
            "count": 6
          }
        }
         */
        resourcesPuller.getSlabRecipes().forEach((fileName, recipe) -> {
            JsonObject recipeJson = JsonParser.parseString(recipe).getAsJsonObject();
            String verticalPattern = "[\"#\",\"#\",\"#\"]";
            recipeJson.add("pattern", GSON.fromJson(verticalPattern, JsonElement.class));
            Identifier originBlockID = new Identifier(recipeJson.get("result").getAsJsonObject().get("item").getAsString());
            Identifier targetBlockID = new Identifier(originBlockID.getNamespace(), "vertical_" + originBlockID.getPath());
            Identifier targetRecipeID = new Identifier(originBlockID.getNamespace(), fileName.replace(originBlockID.getPath(), targetBlockID.getPath()));
            recipeJson.addProperty("group", targetBlockID.toString());
            recipeJson.get("result").getAsJsonObject().addProperty("item", targetBlockID.toString());
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                    massageIdentifierPath(targetRecipeID, "recipes"),
                    toByteArray(recipeJson.toString())
            );

            /*  ==========  FULL BLOCK RECIPE   ==========
            {
              "type": "minecraft:crafting_shaped",
              "group": "minecraft:stone",
              "pattern": [
                "#",
                "#"
              ],
              "key": {
                "#": {
                  "item": "minecraft:stone_slab"
                }
              },
              "result": {
                "item": "minecraft:stone"
              }
            }

            {
              "type": "minecraft:crafting_shaped",
              "group": "minecraft:stone",
              "pattern": [
                "##"
              ],
              "key": {
                "#": {
                  "item": "minecraft:vertical_stone_slab"
                }
              },
              "result": {
                "item": "minecraft:stone"
              }
            }
             */
            var key = recipeJson.get("key").getAsJsonObject().get("#");
            String item;
            if (key.isJsonArray()) {
                item = key.getAsJsonArray().get(0).getAsJsonObject().get("item").getAsString();
            } else {
                item = key.getAsJsonObject().get("item").getAsString();
            }
            Identifier fullBlockID = new Identifier(item);
            String slabToBlockJson = "{\n" +
                    "              \"type\": \"minecraft:crafting_shaped\",\n" +
                    "              \"group\": \"" + fullBlockID + "\",\n" +
                    "              \"pattern\": [\n" +
                    "                \"#\",\n" +
                    "                \"#\"\n" +
                    "              ],\n" +
                    "              \"key\": {\n" +
                    "                \"#\": {\n" +
                    "                  \"item\": \"" + originBlockID + "\"\n" +
                    "                }\n" +
                    "              },\n" +
                    "              \"result\": {\n" +
                    "                \"item\": \"" + fullBlockID + "\"\n" +
                    "              }\n" +
                    "            }";
            String verticalSlabToBlockJson = "{\n" +
                    "              \"type\": \"minecraft:crafting_shaped\",\n" +
                    "              \"group\": \"" + fullBlockID + "\",\n" +
                    "              \"pattern\": [\n" +
                    "                \"##\"\n" +
                    "              ],\n" +
                    "              \"key\": {\n" +
                    "                \"#\": {\n" +
                    "                  \"item\": \"" + targetBlockID + "\"\n" +
                    "                }\n" +
                    "              },\n" +
                    "              \"result\": {\n" +
                    "                \"item\": \"" + fullBlockID + "\"\n" +
                    "              }\n" +
                    "            }";
            Identifier fromSlabRecipeID = new Identifier(fullBlockID.getNamespace(), fullBlockID.getPath() + "_from_" + originBlockID.getPath() + "s");
            Identifier fromVerticalSlabRecipeID = new Identifier(fullBlockID.getNamespace(), fullBlockID.getPath() + "_from_" + targetBlockID.getPath() + "s");
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                    massageIdentifierPath(fromSlabRecipeID, "recipes"),
                    toByteArray(slabToBlockJson)
            );
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                    massageIdentifierPath(fromVerticalSlabRecipeID, "recipes"),
                    toByteArray(verticalSlabToBlockJson)
            );
        });
    }

    private void createStonecuttingRecipes() {
        /*  ==========  STONECUTTING RECIPE ==========
        {
          "type": "minecraft:stonecutting",
          "ingredient": {
            "item": "minecraft:stone"
          },
          "result": "minecraft:vertical_stone_slab",
          "count": 2
        }
         */
        resourcesPuller.getSlabStonecuttingRecipes().forEach((fileName, recipe) -> {
            JsonObject recipeJson = JsonParser.parseString(recipe).getAsJsonObject();
            Identifier originBlockID = new Identifier(recipeJson.get("result").getAsString());
            Identifier targetBlockID = new Identifier(originBlockID.getNamespace(), "vertical_" + originBlockID.getPath());
            Identifier targetRecipeID = new Identifier(originBlockID.getNamespace(), fileName.replace(originBlockID.getPath(), targetBlockID.getPath()));
            recipeJson.add("result", GSON.toJsonTree(targetBlockID.toString()));
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                    massageIdentifierPath(targetRecipeID, "recipes"),
                    toByteArray(recipeJson.toString())
            );
        });

        // TODO: data/minecraft/advancements/recipes/building_blocks/andesite_slab_from_andesite_stonecutting.json
    }

    private void createOtherRecipes() {
        /*  ==========  MISC RECIPES    ==========
         * Waxed recipes
         * */
        resourcesPuller.getMiscSlabRecipes().forEach((fileName, recipe) -> {
            JsonObject recipeJson = JsonParser.parseString(recipe).getAsJsonObject();

            AtomicReference<Identifier> originBlockID = new AtomicReference<>();
            AtomicReference<Identifier> targetBlockID = new AtomicReference<>();

            if (fileName.contains("waxed") && fileName.contains("from_honeycomb")) {
                var ingredients = recipeJson.getAsJsonArray("ingredients");
                ingredients.forEach(ingredient -> {
                    var item = ingredient.getAsJsonObject().get("item").getAsString();
                    if (item.contains("_slab")) {
                        originBlockID.set(new Identifier(item));
                        targetBlockID.set(new Identifier(originBlockID.get().getNamespace(), "vertical_" + originBlockID.get().getPath()));
                        ingredient.getAsJsonObject().add("item", GSON.toJsonTree(targetBlockID.get().toString()));
                    }
                });
                var resultMap = GSON.fromJson(recipeJson.get("result"), Map.class);
                resultMap.put("item", targetBlockID.toString());
                recipeJson.add("result", GSON.toJsonTree(resultMap));
            } // else if: other weird recipes....

            Identifier targetRecipeID = new Identifier(originBlockID.get().getNamespace(), fileName.replace(originBlockID.get().getPath(), targetBlockID.get().getPath()));

            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                    massageIdentifierPath(targetRecipeID, "recipes"),
                    toByteArray(recipeJson.toString())
            );
        });
    }

    private void createSlabRecipeTriggers() {
        resourcesPuller.getSlabRecipeTriggers().forEach((fileName, recipe) -> {
            JsonObject recipeJson = JsonParser.parseString(recipe).getAsJsonObject();
            JsonArray recipesList = recipeJson
                    .get("rewards").getAsJsonObject()
                    .get("recipes").getAsJsonArray();
            Identifier originBlockID = new Identifier(recipesList.remove(0).getAsString());
            Identifier targetBlockID = new Identifier(originBlockID.getNamespace(), "vertical_" + originBlockID.getPath());
            Identifier targetRecipeID = new Identifier(originBlockID.getNamespace(), fileName.replace(originBlockID.getPath(), targetBlockID.getPath()));
            recipesList.add(targetBlockID.toString());
            recipeJson.get("criteria").getAsJsonObject()
                    .get("has_the_recipe").getAsJsonObject()
                    .get("conditions").getAsJsonObject()
                    .addProperty("recipe", targetRecipeID.toString());
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE_TRIGGER,
                    massageIdentifierPath(targetRecipeID, "advancements/recipes/building_blocks"),
                    toByteArray(recipeJson.toString())
            );
        });
    }

    private void createLootTableResource() {
        /*  ==========      LOOT TABLE      ==========
        {
          "type": "minecraft:block",
          "pools": [
            {
              "rolls": 1.0,
              "bonus_rolls": 0.0,
              "entries": [
                {
                  "type": "minecraft:item",
                  "functions": [
                    {
                      "function": "minecraft:set_count",
                      "conditions": [
                        {
                          "condition": "minecraft:block_state_property",
                          "block": "minecraft:stone_slab",
                          "properties": {
                            "type": "double"
                          }
                        }
                      ],
                      "count": 2.0,
                      "add": false
                    },
                    {
                      "function": "minecraft:explosion_decay"
                    }
                  ],
                  "name": "minecraft:stone_slab"
                }
              ]
            }
          ]
        }
        */

        resourcesPuller.getLootTables().forEach((fileName, lootTable) -> {
            JsonObject lootTableJson = JsonParser.parseString(lootTable).getAsJsonObject();
            String originBlockName = lootTableJson
                    .get("pools").getAsJsonArray().get(0).getAsJsonObject()
                    .get("entries").getAsJsonArray().get(0).getAsJsonObject()
                    .get("name").getAsString();
            Identifier originBlockID = new Identifier(originBlockName);
            Identifier targetBlockID = new Identifier(originBlockID.getNamespace(), "vertical_" + originBlockID.getPath());
            String targetLootTable = lootTable.replace(originBlockID.toString(), targetBlockID.toString());
            DATA_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.LOOT_TABLE,
                    massageIdentifierPath(targetBlockID, "loot_tables/blocks"),
                    toByteArray(targetLootTable)
            );
        });
    }
}
