package com.mojomods.slabby.resources;

import com.mojomods.slabby.blocks.VerticalOxidizableSlabBlock;
import com.mojomods.slabby.blocks.VerticalSlabBlock;
import net.fabricmc.fabric.api.registry.OxidizableBlocksRegistry;
import net.minecraft.block.Oxidizable;
import net.minecraft.util.registry.Registry;

import java.util.List;

import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;

public class SlabbyOxidizableGenerator {
    public void generate() {
        logDebug("Oxidizable Generator - Start");
        var start = System.currentTimeMillis();

        List<VerticalSlabBlock> allVSlabs = Registry.BLOCK.stream()
                .filter(block -> block instanceof VerticalSlabBlock)
                .map(block -> (VerticalSlabBlock) block)
                .toList();

        List<VerticalOxidizableSlabBlock> allVOSlabs = Registry.BLOCK.stream()
                .filter(block -> block instanceof VerticalOxidizableSlabBlock)
                .map(block -> (VerticalOxidizableSlabBlock) block)
                .toList();

        allVOSlabs.stream()
                .filter(voSlab -> voSlab.getDegradationLevel().equals(Oxidizable.OxidationLevel.UNAFFECTED))
                .forEach(baseSlab -> {
                    int valSize = Oxidizable.OxidationLevel.values().length;
                    var baseNameSplit = baseSlab.getLootTableId().getPath().split("/");
                    String baseName = baseNameSplit[baseNameSplit.length - 1];
                    VerticalOxidizableSlabBlock currSlab = baseSlab;
                    for (int i = 0; i < valSize - 1; i++) {
                        Oxidizable.OxidationLevel nextLevel = Oxidizable.OxidationLevel.values()[i + 1];
                        VerticalOxidizableSlabBlock nextSlab = (VerticalOxidizableSlabBlock) allVOSlabs.stream()
                                .filter(block -> oxidizableNameMatch(baseName, block.getLootTableId().getPath()))
                                .filter(block -> block.getDegradationLevel() == nextLevel)
                                .toArray()[0];
                        var currName = currSlab.getLootTableId().getPath();
                        VerticalSlabBlock waxedSlab = (VerticalSlabBlock) allVSlabs.stream()
                                .filter(block -> block.getLootTableId().getPath().contains("waxed_"))
                                .filter(block -> waxedNameMatch(currName, block.getLootTableId().getPath()))
                                .toArray()[0];
                        OxidizableBlocksRegistry.registerOxidizableBlockPair(currSlab, nextSlab);
                        OxidizableBlocksRegistry.registerWaxableBlockPair(currSlab, waxedSlab);
                        currSlab = nextSlab;
                    }
                });

        logDebug("Oxidizable Generator - Success: " + (System.currentTimeMillis() - start) + "ms");
    }

    private boolean oxidizableNameMatch(String baseName, String nextName) {
        var nextNameSplit = nextName.split("/");
        String nextBlockName = nextNameSplit[nextNameSplit.length - 1];
        for (String s : baseName.split("_")) {
            if (!nextBlockName.contains(s)) return false;
        }
        return true;
    }

    private boolean waxedNameMatch(String baseName, String waxedName) {
        var baseNameSplit = baseName.split("/");
        String baseBlockName = baseNameSplit[baseNameSplit.length - 1];
        var waxedNameSplit = waxedName.split("/");
        String waxedBlockName = waxedNameSplit[waxedNameSplit.length - 1];
        for (String s : baseBlockName.split("_")) {
            if (!waxedBlockName.contains(s)) return false;
        }
        return true;
    }
}
