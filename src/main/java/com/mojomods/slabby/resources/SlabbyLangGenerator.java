package com.mojomods.slabby.resources;

import com.google.gson.Gson;
import net.minecraft.util.Identifier;

import java.util.HashMap;
import java.util.Map;

import static com.mojomods.slabby.Slabby.ASSETS_RESOURCE_PACK;
import static com.mojomods.slabby.Slabby.MOD_ID;
import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;
import static com.mojomods.slabby.util.SlabbyStringUtils.*;

public class SlabbyLangGenerator {

    private final Gson GSON = new Gson();

    private final Map<String, String> verticalSlabNames = new HashMap<>();

    public void generate() {
        logDebug("Lang Generator - Start");
        var start = System.currentTimeMillis();
        String langJson = GSON.toJson(verticalSlabNames);
        ASSETS_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.LANG,
                massageIdentifierPath(new Identifier(MOD_ID, "en_us"), "lang"),
                toByteArray(langJson)
        );
        logDebug("Lang Generator - Success: " + (System.currentTimeMillis() - start) + "ms");
    }

    public void addLang(Identifier id) {
        String namespace = id.getNamespace();
        String name = id.getPath();
        String blockName = getIdentifierName(id);
        verticalSlabNames.put("item." + namespace + "." + name, blockName);
        verticalSlabNames.put("block." + namespace + "." + name, blockName);
    }
}
