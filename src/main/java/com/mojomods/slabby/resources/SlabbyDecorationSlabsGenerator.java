package com.mojomods.slabby.resources;

import com.mojomods.slabby.blocks.GlassSlabBlock;
import com.mojomods.slabby.blocks.StainedGlassSlabBlock;
import com.mojomods.slabby.blocks.TintedGlassSlabBlock;
import com.mojomods.slabby.util.SlabbyResourceUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.registry.FlammableBlockRegistry;
import net.fabricmc.fabric.impl.content.registry.FlammableBlockRegistryImpl;
import net.fabricmc.fabric.mixin.object.builder.AbstractBlockSettingsAccessor;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.*;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.io.IOException;
import java.util.Arrays;

import static com.mojomods.slabby.Slabby.*;
import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;
import static com.mojomods.slabby.util.SlabbyLogUtils.logError;
import static com.mojomods.slabby.util.SlabbyStringUtils.*;

public class SlabbyDecorationSlabsGenerator {
    private final SlabbyVanillaResourcesPuller resourcesPuller;
    private final SlabbyLangGenerator langGenerator;
    private final SlabbyTagsGenerator tagsGenerator;

    public SlabbyDecorationSlabsGenerator(
            SlabbyVanillaResourcesPuller resourcesPuller,
            SlabbyLangGenerator langGenerator,
            SlabbyTagsGenerator tagsGenerator
    ) {
        this.resourcesPuller = resourcesPuller;
        this.langGenerator = langGenerator;
        this.tagsGenerator = tagsGenerator;
    }

    public void generate() {
        logDebug("Decoration Slabs Generator - Start");
        var start = System.currentTimeMillis();
        Registry.ITEM.stream()
                .filter(item -> Registry.ITEM.getId(item).getNamespace().equals("minecraft")) // vanilla only
                .filter(item -> item instanceof BlockItem)
                .map(item -> (BlockItem) item)
                .filter(this::blockIsDecorationBlock)
                .forEach(this::generateSlabs);
        logDebug("Decoration Slabs Generator - Success: " + (System.currentTimeMillis() - start) + "ms");
    }

    private boolean blockIsDecorationBlock(BlockItem item) {
        String path = item.getBlock().getLootTableId().getPath();
        return (path.endsWith("_concrete")
                || path.endsWith("glass")
                || path.endsWith("terracotta")
                || path.endsWith("wool"))
                    && !path.contains("glazed");
    }

    private void generateSlabs(BlockItem originItem) {
        var originBlock = originItem.getBlock();
        if (originBlock instanceof GlassBlock) {
            generateGlassSlabBlock(originItem);
        } else if (originBlock instanceof TintedGlassBlock) {
            generateTintedGlassSlabBlock(originItem);
        } else if (originBlock instanceof StainedGlassBlock) {
            generateStainedGlassSlabBlock(originItem);
        } else {
            generateSlabBlock(originItem);
        }
    }

    private void generateGlassSlabBlock(BlockItem originItem) {
        GlassBlock originBlock = (GlassBlock) originItem.getBlock(); // cast check made before function call
        FabricBlockSettings settings = FabricBlockSettings.copyOf(originBlock);
        GlassSlabBlock targetBlock = new GlassSlabBlock(settings
                .nonOpaque()
                .blockVision(SlabbyResourceUtils::never)
                .allowsSpawning(SlabbyResourceUtils::never)
                .solidBlock(SlabbyResourceUtils::never)
                .suffocates(SlabbyResourceUtils::never));
        generateSlabResources(originItem, originBlock, targetBlock);
    }

    private void generateTintedGlassSlabBlock(BlockItem originItem) {
        TintedGlassBlock originBlock = (TintedGlassBlock) originItem.getBlock(); // cast check made before function call
        FabricBlockSettings settings = FabricBlockSettings.copyOf(originBlock);
        TintedGlassSlabBlock targetBlock = new TintedGlassSlabBlock(settings
                .nonOpaque()
                .blockVision(SlabbyResourceUtils::never)
                .allowsSpawning(SlabbyResourceUtils::never)
                .solidBlock(SlabbyResourceUtils::never)
                .suffocates(SlabbyResourceUtils::never));
        generateSlabResources(originItem, originBlock, targetBlock);
    }

    private void generateStainedGlassSlabBlock(BlockItem originItem) {
        StainedGlassBlock originBlock = (StainedGlassBlock) originItem.getBlock(); // cast check made before function call
        FabricBlockSettings settings = FabricBlockSettings.copyOf(originBlock);
        StainedGlassSlabBlock targetBlock = new StainedGlassSlabBlock(
                originBlock.getColor(),
                settings.nonOpaque()
                        .blockVision(SlabbyResourceUtils::never)
                        .allowsSpawning(SlabbyResourceUtils::never)
                        .solidBlock(SlabbyResourceUtils::never)
                        .suffocates(SlabbyResourceUtils::never));
        generateSlabResources(originItem, originBlock, targetBlock);
    }

    private void generateSlabBlock(BlockItem originItem) {
        Block originBlock = originItem.getBlock();
        FabricBlockSettings settings = FabricBlockSettings.copyOf(originBlock);
        SlabBlock targetBlock = new SlabBlock(settings);

        // This only needs to be here for the wool
        if (((AbstractBlockSettingsAccessor) settings).getMaterial().isBurnable()) {
            var flammableBlockRegistry = (FlammableBlockRegistryImpl) FlammableBlockRegistry.getDefaultInstance();
            var originFlammable = flammableBlockRegistry.get(originBlock);
            flammableBlockRegistry.add(targetBlock, originFlammable.getBurnChance(), originFlammable.getSpreadChance());
        }

        generateSlabResources(originItem, originBlock, targetBlock);
    }

    private void generateSlabResources(BlockItem originItem, Block originBlock, Block targetBlock) {
        Identifier originBlockID = Registry.BLOCK.getId(originBlock);
        String namespace = originBlockID.getNamespace();
        String originName = originBlockID.getPath();

        String targetBlockName = originName + "_slab";
        Identifier targetBlockID = new Identifier(namespace, targetBlockName);

        if (Registry.BLOCK.containsId(targetBlockID)) return; // can't double up on blocks

        // Item Settings
        FabricItemSettings targetItemSettings = new FabricItemSettings();
        targetItemSettings.maxDamage(originItem.getMaxDamage());
        targetItemSettings.maxCount(originItem.getMaxCount());
        targetItemSettings.recipeRemainder(originItem.getRecipeRemainder());
        targetItemSettings.group(originItem.getGroup());
        targetItemSettings.rarity(originItem.getRarity(originItem.getDefaultStack()));
        targetItemSettings.food(originItem.getFoodComponent());
        if (originItem.isFireproof()) targetItemSettings.fireproof();

        try {
            if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) {
                String texture = SlabbyTexturesPuller.pullBlockTexture(originBlockID);
                generateClientResources(originBlockID, targetBlockID, texture);
                if (targetBlock instanceof StainedGlassSlabBlock || targetBlock instanceof TintedGlassSlabBlock)
                    BlockRenderLayerMap.INSTANCE.putBlock(targetBlock, RenderLayer.getTranslucent());
                else if (targetBlock instanceof GlassSlabBlock)
                    BlockRenderLayerMap.INSTANCE.putBlock(targetBlock, RenderLayer.getCutout());
            }
        } catch (IOException e) {
            logError("Unable to pull block textures for " + originBlockID);
            logError(e.getMessage());
        }

        generateServerResources(originBlockID, targetBlockID);

        Registry.register(Registry.BLOCK, targetBlockID, targetBlock);
        Registry.register(Registry.ITEM, targetBlockID, new BlockItem(targetBlock, targetItemSettings));
    }

    @Environment(EnvType.CLIENT)
    private void generateClientResources(Identifier originBlockID, Identifier targetBlockID, String texture) {
        String targetName = targetBlockID.getPath();
        String originBlockPathName = "block/" + originBlockID.getPath();
        String targetBlockPathName = "block/" + targetName;
        String targetItemPathName = "item/" + targetName;

        createBlockModelResource(texture, targetBlockPathName);
        createItemModelResource(targetBlockPathName, targetItemPathName);
        createBlockStateResources(targetBlockID, targetBlockPathName, originBlockPathName);

        /*  ==========          LANG        ==========
            "item.minecraft.white_concrete_slab": "White Concrete Slab",
            "block.minecraft.white_concrete_slab": "White Concrete Slab"
         */
        langGenerator.addLang(targetBlockID);
    }

    private void generateServerResources(Identifier originBlockID, Identifier targetBlockID) {
        createNormalRecipes(originBlockID, targetBlockID);
        if (originBlockID.getPath().contains("concrete") || originBlockID.getPath().contains("terracotta"))
            createStonecuttingRecipes(originBlockID, targetBlockID);
        if (originBlockID.getPath().contains("glass"))
            createGlassLootTableResource(targetBlockID);
        else
            createLootTableResource(targetBlockID);
        createToolTagsResources(originBlockID, targetBlockID);
    }

    private void createBlockModelResource(String texture, String targetBlockPathName) {
        /*  ==========      BLOCK MODEL     ==========
        {
          "parent": "minecraft:block/slab",
          "textures": {
            "bottom": "minecraft:block/white_concrete",
            "top": "minecraft:block/white_concrete",
            "side": "minecraft:block/white_concrete"
          }
        }
        {
          "parent": "minecraft:block/slab_top",
          "textures": {
            "bottom": "minecraft:block/white_concrete",
            "top": "minecraft:block/white_concrete",
            "side": "minecraft:block/white_concrete"
          }
        }
         */
        Arrays.stream(new String[] { "", "_top" }).forEach(dir -> {
            String slabJson = "{\n" +
                    "          \"parent\": \"minecraft:block/slab" + dir + "\",\n" +
                    "          \"textures\": {\n" +
                    "            \"top\": \"" + texture + "\",\n" +
                    "            \"side\": \"" + texture + "\",\n" +
                    "            \"bottom\": \"" + texture + "\"\n" +
                    "          }\n" +
                    "        }";
            String path = targetBlockPathName + dir;
            ASSETS_RESOURCE_PACK.addResource(
                    SlabbyDynamicResourcePack.SlabbyDynamicResourceType.MODEL,
                    massageIdentifierPath(new Identifier("minecraft", path), "models"),
                    toByteArray(slabJson)
            );
        });
    }

    private void createItemModelResource(String targetBlockPathName, String targetItemPathName) {
        /*  ==========      ITEM MODEL     ==========
        {
          "parent": "block/vertical_stone_slab"
        }
         */
        String itemJson = "{ \"parent\": \"" + targetBlockPathName + "\" }";
        ASSETS_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.MODEL,
                massageIdentifierPath(new Identifier("minecraft", targetItemPathName), "models"),
                toByteArray(itemJson)
        );
    }

    private void createBlockStateResources(Identifier targetBlockID, String targetBlockPathName, String originBlockPathName) {
        /*  ==========      BLOCK STATE     ==========
        {
          "variants": {
            "type=bottom": {
              "model": "minecraft:block/white_concrete_slab"
            },
            "type=top": {
              "model": "minecraft:block/white_concrete_slab_top"
            },
            "type=double": {
              "model": "minecraft:block/white_concrete"
            }
          }
        }
         */
        String blockState = "{\n" +
                "          \"variants\": {\n" +
                "            \"type=bottom\": {\n" +
                "              \"model\": \"minecraft:" + targetBlockPathName + "\"\n" +
                "            },\n" +
                "            \"type=top\": {\n" +
                "              \"model\": \"minecraft:" + targetBlockPathName + "_top\"\n" +
                "            },\n" +
                "            \"type=double\": {\n" +
                "              \"model\": \"minecraft:" + originBlockPathName + "\"\n" +
                "            }\n" +
                "          }\n" +
                "        }";
        ASSETS_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.BLOCK_STATE,
                massageIdentifierPath(targetBlockID, "blockstates"),
                toByteArray(blockState)
        );
    }

    private void createNormalRecipes(Identifier originBlockID, Identifier targetBlockID) {
        /*  ==========  NORMAL RECIPE   ==========
        {
          "type": "minecraft:crafting_shaped",
          "pattern": [
            "###"
          ],
          "key": {
            "#": {
              "item": "minecraft:white_concrete"
            }
          },
          "result": {
            "item": "minecraft:white_concrete_slab",
            "count": 6
          }
        }
         */
        String recipeJson = "{" +
                "\"type\": \"minecraft:crafting_shaped\",\n" +
                "\"pattern\": [\n" +
                "  \"###\"\n" +
                "],\n" +
                "\"key\": {\n" +
                "  \"#\": {\n" +
                "    \"item\": \"" + originBlockID + "\"\n" +
                "  }\n" +
                "},\n" +
                "\"result\": {\n" +
                "  \"item\": \"" + targetBlockID + "\",\n" +
                "\"count\": 6\n" +
                "}}";

        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                massageIdentifierPath(targetBlockID, "recipes"),
                toByteArray(recipeJson)
        );

        resourcesPuller.addSlabRecipe(targetBlockID.getPath(), recipeJson);

        /*  ==========  RECIPE TRIGGER  ==========
        {
          "parent": "minecraft:recipes/root",
          "rewards": {
            "recipes": [
              "minecraft:white_concrete_slab"
            ]
          },
          "criteria": {
            "has_the_block": {
              "trigger": "minecraft:inventory_changed",
              "conditions": {
                "items": [
                  {
                    "items": [
                      "minecraft:white_concrete"
                    ]
                  }
                ]
              }
            },
            "has_the_recipe": {
              "trigger": "minecraft:recipe_unlocked",
              "conditions": {
                "recipe": "minecraft:white_concrete_slab"
              }
            }
          },
          "requirements": [
            [
              "has_the_block",
              "has_the_recipe"
            ]
          ]
        }
         */
        String recipeTriggerJson = "{\n" +
                "          \"parent\": \"minecraft:recipes/root\",\n" +
                "          \"rewards\": {\n" +
                "            \"recipes\": [\n" +
                "              \"" + targetBlockID + "\"\n" +
                "            ]\n" +
                "          },\n" +
                "          \"criteria\": {\n" +
                "            \"has_the_block\": {\n" +
                "              \"trigger\": \"minecraft:inventory_changed\",\n" +
                "              \"conditions\": {\n" +
                "                \"items\": [\n" +
                "                  {\n" +
                "                    \"items\": [\n" +
                "                      \"" + originBlockID + "\"\n" +
                "                    ]\n" +
                "                  }\n" +
                "                ]\n" +
                "              }\n" +
                "            },\n" +
                "            \"has_the_recipe\": {\n" +
                "              \"trigger\": \"minecraft:recipe_unlocked\",\n" +
                "              \"conditions\": {\n" +
                "                \"recipe\": \"" + targetBlockID + "\"\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"requirements\": [\n" +
                "            [\n" +
                "              \"has_the_block\",\n" +
                "              \"has_the_recipe\"\n" +
                "            ]\n" +
                "          ]\n" +
                "        }";
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE_TRIGGER,
                massageIdentifierPath(targetBlockID, "advancements/recipes/building_blocks"),
                toByteArray(recipeTriggerJson)
        );

        resourcesPuller.addSlabRecipeTrigger(targetBlockID.getPath(), recipeTriggerJson);
    }

    private void createStonecuttingRecipes(Identifier originBlockID, Identifier targetBlockID) {
        /*  ==========  STONECUTTING RECIPE ==========
        {
          "type": "minecraft:stonecutting",
          "ingredient": {
            "item": "minecraft:stone"
          },
          "result": "minecraft:vertical_stone_slab",
          "count": 2
        }
         */
        String recipeJson = "{\n" +
                "          \"type\": \"minecraft:stonecutting\",\n" +
                "          \"ingredient\": {\n" +
                "            \"item\": \"" + originBlockID + "\"\n" +
                "          },\n" +
                "          \"result\": \"" + targetBlockID + "\",\n" +
                "          \"count\": 2\n" +
                "        }";

        Identifier targetRecipeID = new Identifier(originBlockID.getNamespace(), targetBlockID.getPath() + "_from_" + originBlockID.getPath() + "_stonecutting");

        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE,
                massageIdentifierPath(targetRecipeID, "recipes"),
                toByteArray(recipeJson)
        );

        resourcesPuller.addStonecuttingRecipe(targetRecipeID.getPath(), recipeJson);

        /*  ==========  RECIPE TRIGGER  ==========
        {
          "parent": "minecraft:recipes/root",
          "rewards": {
            "recipes": [
              "minecraft:white_concrete_slab_from_stonecutting"
            ]
          },
          "criteria": {
            "has_the_block": {
              "trigger": "minecraft:inventory_changed",
              "conditions": {
                "items": [
                  {
                    "items": [
                      "minecraft:white_concrete"
                    ]
                  }
                ]
              }
            },
            "has_the_recipe": {
              "trigger": "minecraft:recipe_unlocked",
              "conditions": {
                "recipe": "minecraft:white_concrete_slab_from_stonecutting"
              }
            }
          },
          "requirements": [
            [
              "has_the_block",
              "has_the_recipe"
            ]
          ]
        }
         */
        String recipeTriggerJson = "{\n" +
                "          \"parent\": \"minecraft:recipes/root\",\n" +
                "          \"rewards\": {\n" +
                "            \"recipes\": [\n" +
                "              \"" + targetRecipeID + "\"\n" +
                "            ]\n" +
                "          },\n" +
                "          \"criteria\": {\n" +
                "            \"has_the_block\": {\n" +
                "              \"trigger\": \"minecraft:inventory_changed\",\n" +
                "              \"conditions\": {\n" +
                "                \"items\": [\n" +
                "                  {\n" +
                "                    \"items\": [\n" +
                "                      \"" + originBlockID + "\"\n" +
                "                    ]\n" +
                "                  }\n" +
                "                ]\n" +
                "              }\n" +
                "            },\n" +
                "            \"has_the_recipe\": {\n" +
                "              \"trigger\": \"minecraft:recipe_unlocked\",\n" +
                "              \"conditions\": {\n" +
                "                \"recipe\": \"" + targetRecipeID + "\"\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"requirements\": [\n" +
                "            [\n" +
                "              \"has_the_block\",\n" +
                "              \"has_the_recipe\"\n" +
                "            ]\n" +
                "          ]\n" +
                "        }";
        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.RECIPE_TRIGGER,
                massageIdentifierPath(targetRecipeID, "advancements/recipes/building_blocks"),
                toByteArray(recipeTriggerJson)
        );

        resourcesPuller.addSlabRecipeTrigger(targetRecipeID.getPath(), recipeTriggerJson);
    }

    private void createLootTableResource(Identifier targetBlockID) {
        /*  ==========      LOOT TABLE      ==========
        {
          "type": "minecraft:block",
          "pools": [
            {
              "rolls": 1.0,
              "bonus_rolls": 0.0,
              "entries": [
                {
                  "type": "minecraft:item",
                  "functions": [
                    {
                      "function": "minecraft:set_count",
                      "conditions": [
                        {
                          "condition": "minecraft:block_state_property",
                          "block": "minecraft:white_concrete_slab",
                          "properties": {
                            "type": "double"
                          }
                        }
                      ],
                      "count": 2.0,
                      "add": false
                    },
                    {
                      "function": "minecraft:explosion_decay"
                    }
                  ],
                  "name": "minecraft:white_concrete_slab"
                }
              ]
            }
          ]
        }
        */

        String lootTableJson = "{\n" +
                "  \"type\": \"minecraft:block\",\n" +
                "  \"pools\": [\n" +
                "    {\n" +
                "      \"rolls\": 1.0,\n" +
                "      \"bonus_rolls\": 0.0,\n" +
                "      \"entries\": [\n" +
                "        {\n" +
                "          \"type\": \"minecraft:item\",\n" +
                "          \"functions\": [\n" +
                "            {\n" +
                "              \"function\": \"minecraft:set_count\",\n" +
                "              \"conditions\": [\n" +
                "                {\n" +
                "                  \"condition\": \"minecraft:block_state_property\",\n" +
                "                  \"block\": \"" + targetBlockID + "\",\n" +
                "                  \"properties\": {\n" +
                "                    \"type\": \"double\"\n" +
                "                  }\n" +
                "                }\n" +
                "              ],\n" +
                "              \"count\": 2.0,\n" +
                "              \"add\": false\n" +
                "            },\n" +
                "            {\n" +
                "              \"function\": \"minecraft:explosion_decay\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"name\": \"" + targetBlockID + "\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.LOOT_TABLE,
                massageIdentifierPath(targetBlockID, "loot_tables/blocks"),
                toByteArray(lootTableJson)
        );
        resourcesPuller.addLootTable(targetBlockID.getPath(), lootTableJson);
    }

    private void createGlassLootTableResource(Identifier targetBlockID) {
        /*  ==========      LOOT TABLE      ==========
        {
          "type": "minecraft:block",
          "pools": [
            {
              "rolls": 1.0,
              "bonus_rolls": 0.0,
              "entries": [
                {
                  "type": "minecraft:item",
                  "functions": [
                    {
                      "function": "minecraft:set_count",
                      "conditions": [
                        {
                          "condition": "minecraft:block_state_property",
                          "block": "minecraft:glass_slab",
                          "properties": {
                            "type": "double"
                          }
                        }
                      ],
                      "count": 2.0,
                      "add": false
                    },
                    {
                      "function": "minecraft:explosion_decay"
                    }
                  ],
                  "name": "minecraft:glass_slab"
                }
              ],
              "conditions": [
                {
                  "condition": "minecraft:match_tool",
                  "predicate": {
                    "enchantments": [
                      {
                        "enchantment": "minecraft:silk_touch",
                        "levels": {
                          "min": 1
                        }
                      }
                    ]
                  }
                }
              ]
            }
          ]
        }
        */

        String glassLootTableJson = "{\n" +
                "  \"type\": \"minecraft:block\",\n" +
                "  \"pools\": [\n" +
                "    {\n" +
                "      \"rolls\": 1.0,\n" +
                "      \"bonus_rolls\": 0.0,\n" +
                "      \"entries\": [\n" +
                "        {\n" +
                "          \"type\": \"minecraft:item\",\n" +
                "          \"functions\": [\n" +
                "            {\n" +
                "              \"function\": \"minecraft:set_count\",\n" +
                "              \"conditions\": [\n" +
                "                {\n" +
                "                  \"condition\": \"minecraft:block_state_property\",\n" +
                "                  \"block\": \"" + targetBlockID + "\",\n" +
                "                  \"properties\": {\n" +
                "                    \"type\": \"double\"\n" +
                "                  }\n" +
                "                }\n" +
                "              ],\n" +
                "              \"count\": 2.0,\n" +
                "              \"add\": false\n" +
                "            },\n" +
                "            {\n" +
                "              \"function\": \"minecraft:explosion_decay\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"name\": \"" + targetBlockID + "\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"conditions\": [\n" +
                "        {\n" +
                "          \"condition\": \"minecraft:match_tool\",\n" +
                "          \"predicate\": {\n" +
                "            \"enchantments\": [\n" +
                "              {\n" +
                "                \"enchantment\": \"minecraft:silk_touch\",\n" +
                "                \"levels\": {\n" +
                "                  \"min\": 1\n" +
                "                }\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        DATA_RESOURCE_PACK.addResource(
                SlabbyDynamicResourcePack.SlabbyDynamicResourceType.LOOT_TABLE,
                massageIdentifierPath(targetBlockID, "loot_tables/blocks"),
                toByteArray(glassLootTableJson)
        );
        resourcesPuller.addLootTable(targetBlockID.getPath(), glassLootTableJson);
    }

    private void createToolTagsResources(Identifier originBlockID, Identifier targetBlockID) {
        String originID = originBlockID.toString();
        String targetID = targetBlockID.toString();
        if (resourcesPuller.needsStoneTool(originID)) {
            tagsGenerator.addNeedsStoneTool(targetBlockID);
            resourcesPuller.addNeedsStoneTool(targetID);
        } else if (resourcesPuller.needsIronTool(originID)) {
            tagsGenerator.addNeedsIronTool(targetBlockID);
            resourcesPuller.addNeedsIronTool(targetID);
        } else if (resourcesPuller.needsDiamondTool(originID)) {
            tagsGenerator.addNeedsDiamondTool(targetBlockID);
            resourcesPuller.addNeedsDiamondTool(targetID);
        }

        if (resourcesPuller.pickaxeMineable(originID)) {
            tagsGenerator.addPickaxeMineable(targetBlockID);
            resourcesPuller.addPickaxeMineable(targetID);
        } else if (resourcesPuller.axeMineable(originID)) {
            tagsGenerator.addAxeMineable(targetBlockID);
            resourcesPuller.addAxeMineable(targetID);
        } else if (resourcesPuller.shovelMineable(originID)) {
            tagsGenerator.addShovelMineable(targetBlockID);
            resourcesPuller.addShovelMineable(targetID);
        } else if (resourcesPuller.hoeMineable(originID)) {
            tagsGenerator.addHoeMineable(targetBlockID);
            resourcesPuller.addHoeMineable(targetID);
        }

        if (resourcesPuller.isWool(originID)) {
            tagsGenerator.addWool(targetBlockID);
            resourcesPuller.addWool(targetID);
        }
    }

}
