package com.mojomods.slabby.resources;

import com.google.gson.JsonObject;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.resource.metadata.ResourceMetadataReader;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class SlabbyDynamicResourcePack implements ResourcePack {
    public enum SlabbyDynamicResourceType {
        MODEL,
        BLOCK_STATE,
        LOOT_TABLE,
        LANG,
        RECIPE,
        TAG,
        RECIPE_TRIGGER,
    }

    private final Map<Identifier, Supplier<byte[]>> serverData = new ConcurrentHashMap<>();
    private final Map<Identifier, Supplier<byte[]>> clientResources = new ConcurrentHashMap<>();
    private final Lock MUTEX = new ReentrantLock();

    private void mutexLock() {
        if(!this.MUTEX.tryLock()) {
            this.MUTEX.lock();
        }
    }

    private Map<Identifier, Supplier<byte[]>> getSource(ResourceType type) {
        return type == ResourceType.CLIENT_RESOURCES ? this.clientResources : this.serverData;
    }

    public byte[] addResource(SlabbyDynamicResourceType type, Identifier path, byte[] resourceData) {
        switch (type) {
            case MODEL, BLOCK_STATE, LANG -> this.getSource(ResourceType.CLIENT_RESOURCES).put(path, () -> resourceData);
            case RECIPE, LOOT_TABLE, TAG, RECIPE_TRIGGER -> this.getSource(ResourceType.SERVER_DATA).put(path, () -> resourceData);
        }

        return resourceData;
    }

    public byte[] getResource(ResourceType type, Identifier id) {
        Map<Identifier, Supplier<byte[]>> resources = type == ResourceType.CLIENT_RESOURCES ? this.clientResources : this.serverData;
        return resources.get(id).get();
    }

    @Nullable
    @Override
    public InputStream openRoot(String fileName) throws IOException { return null; }

    @Override
    public InputStream open(ResourceType type, Identifier id) {
        this.mutexLock();
        Supplier<byte[]> supplier = this.getSource(type).get(id);
        if(supplier == null) {
            this.MUTEX.unlock();
            return null;
        }
        this.MUTEX.unlock();
        return new ByteArrayInputStream(supplier.get());
    }

    @Override
    public Collection<Identifier> findResources(ResourceType type, String namespace, String prefix, int maxDepth, Predicate<String> pathFilter) {
        this.mutexLock();
        Set<Identifier> identifiers = new HashSet<>();
        for(Identifier identifier : this.getSource(type).keySet()) {
            if(identifier.getNamespace().equals(namespace) && identifier.getPath().startsWith(prefix) && pathFilter.test(identifier.getPath())) {
                identifiers.add(identifier);
            }
        }
        this.MUTEX.unlock();
        return identifiers;
    }

    @Override
    public boolean contains(ResourceType type, Identifier id) {
        this.mutexLock();
        boolean contains = this.getSource(type).containsKey(id);
        this.MUTEX.unlock();
        return contains;
    }

    @Override
    public Set<String> getNamespaces(ResourceType type) {
        this.mutexLock();
        Set<String> namespaces = new HashSet<>();
        for(Identifier identifier : this.getSource(type).keySet()) {
            namespaces.add(identifier.getNamespace());
        }
        this.MUTEX.unlock();
        return namespaces;
    }

    @Nullable
    @Override
    public <T> T parseMetadata(ResourceMetadataReader<T> metaReader) throws IOException {
        return metaReader.fromJson(new JsonObject());
    }

    @Override
    public String getName() {
        return "Slabby Dynamic Resource Pack";
    }

    @Override
    public void close() {}
}
