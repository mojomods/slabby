package com.mojomods.slabby.enums;

import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.StringIdentifiable;

/**
 * @see net.minecraft.block.enums.SlabType
 */
public enum VerticalSlabType implements StringIdentifiable {
    NORTH("north"),
    EAST("east"),
    SOUTH("south"),
    WEST("west"),
    DOUBLE("double"),
    ;

    private final String name;

    private VerticalSlabType(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public String asString() {
        return this.name;
    }

    public static EnumProperty<VerticalSlabType> getEnumProperty() {
        return EnumProperty.of("type", VerticalSlabType.class);
    }
}
