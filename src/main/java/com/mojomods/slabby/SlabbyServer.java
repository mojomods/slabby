package com.mojomods.slabby;

import net.fabricmc.api.DedicatedServerModInitializer;

public class SlabbyServer implements DedicatedServerModInitializer {
    @Override
    public void onInitializeServer() {
        SlabbyConnectionManager.initServer();
    }
}
