package com.mojomods.slabby;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginNetworking;
import net.fabricmc.fabric.api.networking.v1.*;
import net.minecraft.server.network.ServerLoginNetworkHandler;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;

public class SlabbyConnectionManager {

    private final static Identifier HANDSHAKE = new Identifier(Slabby.MOD_ID, "ping");

    @Environment(EnvType.SERVER)
    public static void initServer() {
        ServerLoginConnectionEvents.QUERY_START.register((handler, server, sender, synchronizer) -> {
            sender.sendPacket(HANDSHAKE, PacketByteBufs.empty());
        });
        ServerLoginNetworking.registerGlobalReceiver(HANDSHAKE, (server, handler, understood, buf, synchronizer, responseSender) -> {
            if (!understood) {
                disconnect(handler);
            }
        });
    }

    @Environment(EnvType.CLIENT)
    public static void initClient() {
        ClientLoginNetworking.registerGlobalReceiver(HANDSHAKE, (client, handler, buf, listenerAdder) -> {
            SlabbyClient.serverIsSlabby = true;
            return CompletableFuture.completedFuture(PacketByteBufs.empty());
        });
    }

    private static void disconnect(ServerLoginNetworkHandler handler) {
        handler.disconnect(new LiteralText("This server requires you to install the Slabby mod to play."));
    }
}
