package com.mojomods.slabby;

import net.fabricmc.api.ClientModInitializer;

public class SlabbyClient implements ClientModInitializer {
    public static boolean serverIsSlabby = false;

    @Override
    public void onInitializeClient() {
        SlabbyConnectionManager.initClient();
    }
}
