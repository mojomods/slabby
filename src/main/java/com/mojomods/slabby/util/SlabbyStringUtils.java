package com.mojomods.slabby.util;

import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;

import static com.mojomods.slabby.util.SlabbyLogUtils.logDebug;

public class SlabbyStringUtils {
    public static Identifier massageIdentifierPath(Identifier id, String prefix) {
        return new Identifier(id.getNamespace(), prefix + "/" + id.getPath() + ".json");
    }

    public static String massageFilePath(ResourceType type, Identifier id) {
        return type.getDirectory() + "/" + id.getNamespace() + "/" + id.getPath();
    }

    public static String massageResourcePath(ResourceType type, Identifier id) {
        return "/" + type.getDirectory() + "/" + id.getNamespace() + "/" + id.getPath();
    }

    public static String getIdentifierName(Identifier id) {
        return String.join(" ", Arrays.stream(id.getPath().split("_")).map(StringUtils::capitalize).toList());
    }

    public static String getBlockPathFromID(Identifier id) {
        var nameSplit = id.getPath().split("/");
        return nameSplit[nameSplit.length - 1];
    }

    public static String getBlockPathFromIDForTextures(Identifier id) {
        var nameSplit = id.getPath().split("/");
        return nameSplit[nameSplit.length - 1].replaceAll("waxed_", "");
    }

    public static String getFileNameFromPath(String file) {
        var pathSplit = file.split("/");
        String fileName = pathSplit[pathSplit.length - 1];
        var nameSplit = fileName.split("\\.");
        return nameSplit[0];
    }

    public static byte[] toByteArray(String json) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        try {
            writer.write(json);
        } catch (Exception e) {
            logDebug(e.getMessage());
        }

        try {
            writer.close();
        } catch (IOException e) {
            logDebug(e.getMessage());
        }
        return outputStream.toByteArray();
    }
}
