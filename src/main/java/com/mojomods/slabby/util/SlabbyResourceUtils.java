package com.mojomods.slabby.util;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;

public class SlabbyResourceUtils {
    public static boolean never(BlockState state, BlockView world, BlockPos pos) { return false; }
    public static Boolean never(BlockState state, BlockView world, BlockPos pos, EntityType<?> type) { return false; }
}
