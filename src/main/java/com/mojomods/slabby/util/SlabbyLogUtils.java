package com.mojomods.slabby.util;

import net.fabricmc.loader.api.FabricLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.mojomods.slabby.Slabby.MOD_LABEL;

public class SlabbyLogUtils {
    private static final Logger LOGGER = LogManager.getLogger();
    public static void log(String msg) { LOGGER.info("[{}]: {}", MOD_LABEL, msg); }
    public static void logDebug(String msg) { if (FabricLoader.getInstance().isDevelopmentEnvironment()) LOGGER.info("[{}] (DEBUG): {}", MOD_LABEL, msg); }
    public static void logError(String msg) { LOGGER.error("[{}] (ERROR): {}", MOD_LABEL, msg); }
}
