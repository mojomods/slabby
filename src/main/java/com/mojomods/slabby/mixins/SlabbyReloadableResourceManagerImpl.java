package com.mojomods.slabby.mixins;

import com.mojomods.slabby.Slabby;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.Unit;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static com.mojomods.slabby.util.SlabbyLogUtils.*;

@Mixin(value = ReloadableResourceManagerImpl.class, priority = 950)
public abstract class SlabbyReloadableResourceManagerImpl {

    @Inject (method = "reload", at = @At (value = "INVOKE", target = "Ljava/util/List;iterator()Ljava/util/Iterator;"))
    private void injectResourcePacksBeforeReload(Executor prepareExecutor, Executor applyExecutor, CompletableFuture<Unit> initialStage, List<ResourcePack> packs, CallbackInfoReturnable<ResourceReload> cir) {
        log("Adding resource pack: " + type);
        switch (type) {
            case CLIENT_RESOURCES -> addPack(Slabby.ASSETS_RESOURCE_PACK);
            case SERVER_DATA -> addPack(Slabby.DATA_RESOURCE_PACK);
        }
    }

    @Inject(method = "reload", at = @At(value = "RETURN", shift = At.Shift.BEFORE))
    private void injectResourcePacksOnReload(Executor prepareExecutor, Executor applyExecutor, CompletableFuture<Unit> initialStage, List<ResourcePack> packs, CallbackInfoReturnable<ResourceReload> cir) {
        // TODO: this can be v2 (modded blocks)
//        if (type == ResourceType.CLIENT_RESOURCES) {
//            if (!Slabby.ASSETS_LOADED) {
//                Slabby.log("Creating vertical versions of slabs");
//
//                Registry.ITEM.stream()
//                        .filter(item -> item instanceof BlockItem)
//                        .filter(item -> ((BlockItem) item).getBlock() instanceof VerticalSlabBlock)
//                        .forEach(item -> generateVerticalSlabResources((BlockItem) item));
//
//                Slabby.ASSETS_LOADED = true;
//            }
//
//            addPack(Slabby.ASSETS_RESOURCE_PACK);
//        }
//
//        if (type == ResourceType.SERVER_DATA) {
////            if (!Slabby.DATA_LOADED) {
////                Slabby.DATA_LOADED = true;
////            }
//            addPack(Slabby.DATA_RESOURCE_PACK);
//        }

    }

//    private void generateVerticalSlabResources(BlockItem targetItem) {
//        VerticalSlabBlock targetBlock = (VerticalSlabBlock) targetItem.getBlock();
//
//        Identifier targetBlockID = Registry.BLOCK.getId(targetBlock);
//
//        try {
//            JsonParser parser = new JsonParser();
//            JsonObject json = parser.parse(new InputStreamReader(
//                    getResource(new Identifier(
//                            targetBlock.parentBlock.getNamespace(),
//                            "models/block/" + targetBlock.parentBlock.getPath().replaceAll("waxed_", "") + ".json")
//                    ).getInputStream(), UTF_8)
//            ).getAsJsonObject().get("textures").getAsJsonObject();
//
//             Slabby.createVerticalSlabResources(targetBlockID, SlabbyBlockTextures.create(json, targetBlockID.getPath()));
//        } catch (Exception e) {
//            Slabby.logError(e.getMessage());
//        }
//    }

    @Final
    @Shadow
    private ResourceType type;

    @Shadow
    public abstract Resource getResource(Identifier id) throws IOException;

    @Shadow
    public abstract void addPack(ResourcePack resourcePack);
}
