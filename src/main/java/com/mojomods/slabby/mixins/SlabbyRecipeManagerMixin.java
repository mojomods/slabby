package com.mojomods.slabby.mixins;

import com.google.gson.JsonElement;
import net.minecraft.block.SlabBlock;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

@Mixin(RecipeManager.class)
public class SlabbyRecipeManagerMixin {

    @Inject(method = "apply", at = @At("HEAD"))
    public void injectSlabRecipeGroups(Map<Identifier, JsonElement> map, ResourceManager resourceManager, Profiler profiler, CallbackInfo info) {
        map.forEach((id, recipe) -> {
            if (id.getNamespace().equals("minecraft") && Registry.BLOCK.get(id) instanceof SlabBlock) {
                recipe.getAsJsonObject().addProperty("group", id.toString());
                map.put(id, recipe);
            }
        });
    }

}
