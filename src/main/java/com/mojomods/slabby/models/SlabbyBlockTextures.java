package com.mojomods.slabby.models;

import com.google.gson.JsonObject;

public class SlabbyBlockTextures {
    public String top;
    public String side;
    public String bottom;
    public String full;

    SlabbyBlockTextures(String top, String side, String bottom, String full) {
        this.top = top;
        this.side = side;
        this.bottom = bottom;
        this.full = full;
    }

    public static SlabbyBlockTextures fromSlab(JsonObject json, String originBlockName) {
        String topTexture = json.get("top").getAsString();
        String sideTexture = json.get("side").getAsString();
        String bottomTexture = json.get("bottom").getAsString();
        String fullTexture = sanitizeBlockName(sideTexture);

        return switch (originBlockName) {
            // TODO: v2 - smooth stone slab wizardry
            case "smooth_stone_slab" -> new SlabbyBlockTextures(bottomTexture, bottomTexture, bottomTexture, bottomTexture);
            case "smooth_quartz_slab" -> new SlabbyBlockTextures(topTexture, sideTexture, bottomTexture, "minecraft:block/smooth_quartz");
            default -> new SlabbyBlockTextures(topTexture, sideTexture, bottomTexture, fullTexture);
        };
    }

    private static String sanitizeBlockName(String blockName) {
        return blockName.replace("_top", "")
                .replace("_side", "")
                .replace("_bottom", "");
    }
}
